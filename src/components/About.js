import React,{useState} from 'react'


export default function About(props) {
// const[myStyle,setMyStyle]=useState({
//     color:'black',
//     backgroundColor:'white',
//     border:'1px solid white'
// })

//const[btn,setBtn]=useState("Enable Dark Mode")

// const toggleStyle=()=>{
//     if(myStyle.color==='white')
//     {
//         setMyStyle({
//             color:'black',
//             backgroundColor:'white',

//         })
//         setBtn("Enable Dark Mode");
//     }
//     else{
//         setMyStyle({
//             color:'white',
//             backgroundColor:'black'
//         })
//         setBtn("Enable Light Mode")
//     }
// }

  let myStyle={
        color:props.mode==='dark'?'white':'#403a3ae0',
        backgroundColor:props.mode==='dark'?'#403a3ae0':'white'
  }


  return (
    <div className="conatiner px-3 py-3" style={myStyle}>
        <h1>About Us</h1>
      <div className="accordion" id="accordionExample">
        <div className="accordion-item">
        <h2 className="accordion-header" id="headingOne">
      <button className="accordion-button" type="button" style={myStyle} data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
        Analyze your text
      </button>
    </h2>
    <div id="collapseOne" className="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
      <div className="accordion-body" style={myStyle}>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea, repellendus.      </div>
    </div>
  </div>
  <div className="accordion-item">
    <h2 className="accordion-header" id="headingTwo">
      <button className="accordion-button collapsed" type="button" style={myStyle} data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
       Free to use
      </button>
    </h2>
    <div id="collapseTwo" className="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
      <div className="accordion-body" style={myStyle}>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque, dolores.      </div>
    </div>
  </div>
  <div className="accordion-item">
    <h2 className="accordion-header" id="headingThree">
      <button className="accordion-button collapsed" type="button" style={myStyle} data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
       Browser Compatible
      </button>
    </h2>
    <div id="collapseThree" className="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
      <div className="accordion-body" style={myStyle}>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum, aliquam!      </div>
    </div>
  </div>
</div>

</div>

   
   
  )
}
