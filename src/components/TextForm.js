import React, {useState} from 'react'

export default function TextForm(props) {
    const [text, setText] = useState('');  //setting state variable

    const handleUpClick=()=>{
       // console.log("Uppercase was clicked " + text);
        let newText=text.toUpperCase();
        setText(newText);
        props.showAlert("Converted to UpperCase!","success");
    }
    const handleOnChange=(event)=>{
        //console.log("Handle On change");
        setText(event.target.value);
    }
    const handleLowClick=()=>{
        let newText=text.toLowerCase();
        setText(newText);
        props.showAlert("Converted to LowerCase!","success");

    }
    const handleClearClick=()=>{
      let newText='';
      setText(newText);
    }
    const handleCopy=()=>{
      navigator.clipboard.writeText(text);
      props.showAlert("Copied to Clipboard","success");
    }
    const handleExtraSpaces=()=>{
      //console.log("extra")
      let newText=text.split(/[ ]+/);  //used regex in js 
      setText(newText.join(" "));

      
    }
  return (
    <>
      <div className="container" style={{color:props.mode==='light'?'black':'white'}}>
        <h3>{props.title}</h3>
        <div className="mb-3">
        <textarea className="form-control" value={text} style={{backgroundColor:props.mode==='light'?'white':'#d5cdcd66' , 
        color:props.mode==='light'?'black':'white'}} placeholder='enter any text' onChange={handleOnChange} id="box" rows="5"></textarea>
        </div>
        <button disabled={text.length===0} className="btn btn-primary my-2 my-1" onClick={handleUpClick}>Convert to Uppercase</button>
        <button disabled={text.length===0} className="btn btn-primary mx-2 my-1" onClick={handleLowClick}>Convert to LowerCase</button>
        <button disabled={text.length===0} className="btn btn-primary mx-2 my-1" onClick={handleClearClick}>Clear Text</button>
        <button disabled={text.length===0} className="btn btn-primary mx-2 my-1" onClick={handleCopy}>Copy</button>
        <button disabled={text.length===0} className="btn btn-primary mx-2 my-1" onClick={handleExtraSpaces}>Remove Extra Spaces</button>

       </div>

       <div className="container my-4" style={{color:props.mode==='light'?'black':'white'}}>
        <h4>Your Text Summary</h4>
        <p> {text.split(/\s+/).filter((element)=>{
          return element.length!==0
        }).length} words, {text.length} characters</p>
        <p>{0.008 * text.split(' ').filter((element)=>{
          return element.length!==0
        }).length} Minutes read</p>
        <h3>Preview</h3>
        <p>{text.length>0?text:"Nothing to Preview"}</p>
       </div>
    </>
    
  )
}
