import { useState } from 'react';
import './App.css';
import About from './components/About';
import Navbar from './components/Navbar';
import TextForm from './components/TextForm';
import Alert from './components/Alert';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function App() {
  const[mode,setMode]=useState('light');  //whether dark mode is enabled or not
  const[alert,setAlert]=useState(null);
  

  const removeBodyClasses=()=>{
    document.body.classList.remove('bg-light');
    document.body.classList.remove('bg-dark');
    document.body.classList.remove('bg-warning');
    document.body.classList.remove('bg-danger');
    document.body.classList.remove('bg-success');
    document.body.classList.remove('bg-primary');

  }

  const toggleMode=(cls)=>{
    removeBodyClasses();
    console.log(cls);
    if(cls==='dark'){
    setMode('dark');
    document.body.style.backgroundColor='gray';
    //showAlert('Dark mode enabled',"success");
    }
    else if(cls==='light'){
      setMode('light');
      document.body.style.backgroundColor='white';
      //showAlert('Light mode enabled',"success");

    }
    else{
      setMode('light');
      document.body.classList.add('bg-'+cls);

    }
  }

  const showAlert=(message,type)=>{
      setAlert({
        msg:message,
        type:type
      })
      setTimeout(() => {
        setAlert(null);
      }, 1500);
  }
  

  return (
  <>
  <Router>
    <Navbar title="TextUtils" mode={mode} toggleMode={toggleMode}/>        {/* Passing props */}
    <Alert alert={alert}/>
    <div className="container my-3">

    <Switch>
          <Route exact path="/about">
            <About mode={mode} />
          </Route>
        <Route exact path="/">
          <TextForm title="Enter the Text to analyze" mode={mode} showAlert={showAlert}/>
        </Route>
    </Switch>
    </div>
  </Router>  
  </>
  );
}

export default App;
